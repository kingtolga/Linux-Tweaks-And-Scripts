# My GitHub Environment: 14/6/2023

Hey there! 👋 

I wanted to share with you some details about my GitHub environment. Specifically, I'll talk about the custom scripts I've created to enhance my workflow and make my life easier. 

My goal is to assist others in setting up their Linux systems. Even though there are already numerous configuration examples available on the internet and GitHub, I believe that my specific configuration may offer some valuable insights. If you encounter any areas that require correction or have any questions, please feel free to reach out to me.

I have successfully tested the following operating systems with the examples provided in my repository:

- MxLinux
- LinuxMint
- Fedora
- RockyLinux 9
- NixOS
- Fedora

Let's dive in!

## Syncing User Home Folder to a Specified Destination

One of the key scripts I've developed is a custom synchronization script. This script allows me to effortlessly sync my user home folder to a specified destination. By running this script, I can ensure that all my important files and configurations are backed up and accessible from any location. This is particularly helpful when I switch machines or need to restore my settings after a fresh installation.

## Assisting with Mounting, Unmounting, and Suspending

To streamline my workflow, I've also developed a set of personal scripts that assist me with mounting, unmounting, and suspending operations. These scripts automate common tasks, saving me time and effort in managing external drives or suspending my system when I step away. By executing these scripts, I can perform these operations with just a single command, making my workflow more efficient.

## Custom NixOS Configuration File with Bluetooth Variables

As an enthusiast of NixOS, a powerful Linux distribution with a declarative approach to system configuration, I've created a custom configuration file tailored to my needs. In this file, I've set specific variables related to Bluetooth devices. By configuring these variables, I can easily manage my Bluetooth devices and ensure a seamless experience.

![Screenshot_20230610_144645](https://github.com/tolgaerok/Linux-Tweaks-And-Scripts/assets/110285959/af6b682f-0ddd-45bc-babc-0584b0e70884)


## Calling Common NixOS Commands with a Custom Script

To further simplify my interactions with NixOS, I've developed a custom script that encapsulates the most frequently used commands. With this script, I can quickly execute common tasks such as updating the system, installing packages, or configuring services. This saves me the hassle of remembering or typing out lengthy commands each time I need to perform these operations.

![1](https://github.com/tolgaerok/Linux-Tweaks-And-Scripts/assets/110285959/ae14cea8-dae9-4ea9-842d-7232e62ca9ff)

## Conclusion

In this blog post, I've highlighted some of the key components of my GitHub environment. From syncing my user home folder to developing scripts for mounting, unmounting, and suspending, to customizing my NixOS configuration file with Bluetooth variables and creating a script for common NixOS commands, these tools greatly enhance my productivity and simplify my workflow.

If you're interested in exploring these scripts or incorporating them into your own environment, feel free to check out my GitHub repository. I hope you find them useful and they inspire you to create your own custom solutions to enhance your development experience!

Happy coding! 😄

![This is an image](https://myoctocat.com/assets/images/base-octocat.svg)

You can use the [editor on GitHub](https://github.com/tolgaerok/SMB_FILES/edit/gh-pages/index.md) to maintain and preview the content for your website in Markdown files.

Whenever you commit to this repository, GitHub Pages will run [Jekyll](https://jekyllrb.com/) to rebuild the pages in your site, from the content in your 

### Jekyll Themes

Your Pages site will use the layout and styles from the Jekyll theme you have selected in your [repository settings](https://github.com/tolgaerok/SMB_FILES/settings/pages). The name of this theme is saved in the Jekyll `_config.yml` configuration file.

### Support or Contact

Having trouble with Pages? Check out our [documentation](https://docs.github.com/categories/github-pages-basics/) or [contact support](https://support.github.com/contact) and we’ll help you sort it out.


